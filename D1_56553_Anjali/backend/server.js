const express = require('express')
const utils=require('./utils')
const cors = require('cors')

const app = express()
app.use(cors())
app.use(express.json())

app.get('/:movietitle', (request, response) => {
  const { name} = request.params

  const connection = utils.openConnection()

  const statement = `
  select * from Movie where movietitle = '${movietitle}' `
  connection.query(statement, (error, result) => {
    connection.end()
     if (result.length > 0) {
      console.log(result.length);
      console.log(result);
      response.send(utils.createResult(error, result));
    } else {
      response.send("movie not found !");
    }
   
  })
})

app.post('/add', (request, response) => {
  const { movietitle, moviereleasedate,movietime,directorname } = request.body

  const connection = utils.openConnection()

  const statement = `
  insert into Movie(movietitle, moviereleasedate,movietime,directorname) values ( ${movietitle},${moviereleasedate},${movietime},${directorname })`
  connection.query(statement, (error, result) => {
    connection.end()
     if (result.length > 0) {
      console.log(result.length);
      console.log(result);
      response.send(utils.createResult(error, result));
    } else {
      response.send("movie not found !");
    }
   
  })
})

app.put('/updatemovie/:name', (request, response) => {
  const { name} =request.params
  const { moviereleasedate,movietime} = request.body

  const connection = utils.openConnection()

  const statement = `
  update Movie set moviereleasedate = ${ moviereleasedate} and movietime = ${movietime} where name=${name}`
  connection.query(statement, (error, result) => {
    connection.end()
     if (result.length > 0) {
      console.log(result.length);
      console.log(result);
      response.send(utils.createResult(error, result));
    } else {
      response.send("movie not found !");
    }
   
  })
})

app.delete('/deletemovie/:name', (request, response) => {
  const { name} =request.params
  const connection = utils.openConnection()

  const statement = `
  delete from movie where name=${name}`
  connection.query(statement, (error, result) => {
    connection.end()
     if (result.length > 0) {
      console.log(result.length);
      console.log(result);
      response.send(utils.createResult(error, result));
    } else {
      response.send("movie not found !");
    }
   
  })
})
app.listen(4000, () => {
  console.log(`server started on port 4000`)
})